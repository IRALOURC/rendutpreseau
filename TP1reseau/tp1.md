# I. Exploration locale en solo

## 1. Affichage d'informations sur la pile TCP/IP locale

```
$ MacBook-Air-de-Iralour:~ claireiralour$ /sbin/ifconfig
ether 7c:04:d0:ce:8c:8a 
inet 10.33.2.53
```
```
$ route get default
gateway: 10.33.3.253
```
### En graphique (GUI : Graphical User Interface):

Dans preference systeme,

Adresse IP: 10.33.2.53.
Adresse Mac: 7c:04:d0:ce:8c:8a
Gateway (routeur): 10.33.3.253

La gateway sert a accéder au reseau wifi.

## 2. Modifications des informations

### A. Modification d'adresse IP (part 1):
```
$ ping 10.33.2.8
PING 10.33.2.8 (10.33.2.8): 56 data bytes
64 bytes from 10.33.2.8: icmp_seq=0 ttl=64 time=34.932 ms
```

La connexion peut etre perdue si une autre personne est sous la meme adresse ip, deux personnes ne peuvent pas en utiliser une en meme temps

### B. Table ARP:
```
$ arp -a
? (10.33.2.53) at 7c:4:d0:ce:8c:8a on en0 ifscope permanent [ethernet]
```
```
$ ping 10.33.0.78
PING 10.33.0.78 (10.33.0.78): 56 data bytes
```
```
$ ping 10.33.2.197
PING 10.33.2.197 (10.33.2.197): 56 data bytes
```
```
$ ping 10.33.3.253
PING 10.33.3.253 (10.33.3.253): 56 data bytes
```
### C. nmap:
```
$ nmap -sn -PE 10.33.0.0/22
Host is up (0.18s latency).
Nmap scan report for 10.33.0.31
Host is up (0.30s latency).
Nmap scan report for 10.33.0.60
Host is up (0.13s latency).
```
ip libre : 10.33.0.54
```
$ nmap -sP 10.33.0.0/22
Nmap done: 1024 IP addresses (30 hosts up) scanned in 40.18 seconds
```
```
$ arp -a
? (10.2.1.255) at ff:ff:ff:ff:ff:ff on vboxnet0 ifscope [ethernet]
? (10.33.0.6) at 84:5c:f3:80:32:7 on en0 ifscope [ethernet]
? (10.33.0.7) at 9c:bc:f0:b6:1b:ed on en0 ifscope [ethernet]
```
### D. Modification d'adresse IP (part 2):

Nouvelle ip: 10.33.3.147
```
$ nmap -sP 10.33.0.0/22
Starting Nmap 7.92 ( https://nmap.org ) at 2021-09-16 10:01 CEST
Nmap scan report for 10.33.0.7
Host is up (0.39s latency).
Nmap scan report for 10.33.0.31
```
```

$ nmap
Nmap 7.92 ( https://nmap.org )
Usage: nmap [Scan Type(s)] [Options] {target specification}
TARGET SPECIFICATION:
  Can pass hostnames, IP addresses, networks, etc.
  Ex: scanme.nmap.org, microsoft.com/24, 192.168.0.1; 10.0.0-255.1-254
  -iL <inputfilename>: Input from list of hosts/networks
  -iR <num hosts>: Choose random targets
  --exclude <host1[,host2][,host3],...>: Exclude hosts/networks
  --excludefile <exclude_file>: Exclude list from file
HOST DISCOVERY:
```
```
$route get default
gateway: 10.33.3.253
en0: flags=8863<UP,BROADCAST,SMART,RUNNING,SIMPLEX,MULTICAST> mtu 1500
	ether 7c:04:d0:ce:8c:8a 
	inet6 fe80::4b1:b075:6854:efc9%en0 prefixlen 64 secured scopeid 0x4 
	inet 10.33.3.147 netmask 0xfffffc00 broadcast 10.33.3.255
	nd6 options=201<PERFORMNUD,DAD>
	media: autoselect
	status: active
```

### 5. Petit chat privé :
```
$ nc 192.168.196.22 8888
netcat, ecoute sur le port numero 8888 stp
netcat, connecte toi au port 8888 de la machine 192.168.196.22 stp
```

















