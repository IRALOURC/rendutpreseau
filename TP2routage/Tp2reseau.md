# TP2

## 1.Echange ARP :
 
### 🌞Générer des requêtes ARP :

```
 $ ping 10.2.1.12
PING 10.2.1.12 (10.2.1.12) 56(84) bytes of data.
64 bytes from 10.2.1.12: icmpseq=1 ttl=64 time=0.660 ms
64 bytes from 10.2.1.12: icmpseq=2 ttl=64 time=0.730 ms
64 bytes from 10.2.1.12: icmpseq=3 ttl=64 time=0.644 ms
64 bytes from 10.2.1.12: icmpseq=4 ttl=64 time=0.801 ms
^C
--- 10.2.1.12 ping statistics ---
4 packets transmitted, 4 received, 0% packet loss, time 3072ms
rtt min/avg/max/mdev = 0.644/0.708/0.801/0.070 ms
```
```
 $ ping 10.2.1.11
PING 10.2.1.11 (10.2.1.11) 56(84) bytes of data.
64 bytes from 10.2.1.11: icmpseq=1 ttl=64 time=0.055 ms
64 bytes from 10.2.1.11: icmpseq=2 ttl=64 time=0.064 ms
64 bytes from 10.2.1.11: icmpseq=3 ttl=64 time=0.051 ms
^C
--- 10.2.1.11 ping statistics ---
3 packets transmitted, 3 received, 0% packet loss, time 2063ms
rtt min/avg/max/mdev = 0.051/0.056/0.064/0.010 ms
```
```
$ arp -a
? (10.2.1.1) at 0a:00:27:00:00:02 [ether] on enp0s8
gateway (10.0.2.2) at 52:54:00:12:35:02 [ether] on enp0s3
? (10.2.1.12) at 08:00:27:d5:7b:fc [ether] on enp0s8
```
```
$ arp -a
```
node 1
```
? (10.2.1.1) at 0a:00:27:00:00:02 [ether] on enp0s8
```
node2 :
```
? (10.2.1.12) at 08:00:27:d5:7b:fc [ether] on enp0s8
```

## 2 Analyse de trames : 

### 🌞Analyse de trames

vider table arp sur ma node.2
```
$ sudo ip -s -s neigh flush all
```
```
$ ping 10.2.1.11
```
```
$ sudo tcpdump -i enp0s8 -w tp2arp.pcap not port 22
dropped privs to tcpdump
tcpdump: listening on enp0s8, link-type EN10MB (Ethernet), capture size 262144 bytes
^C12 packets captured
12 packets received by filter
0 packets dropped by kernel
```

Nom fichier : tp2arp.pcap

| ordre | type trame  | source                   | destination                |
|-------|-------------|--------------------------|----------------------------|
| 1     | Requête ARP | PcsCompud5:7b:fc        | Broadcast                  |
| 2     | Réponse ARP | PcsCompu70:cb:7d        | PcsCompud5:7b:fc          |


# II. Routage : 

## 1. Mise en place du routage : 

### 🌞Activer le routage sur le noeud router.net2.tp2
```
$ sysctl -w net.ipv4.ipforward=1
$ sudo firewall-cmd --add-masquerade --zone=public --permanent
```

### 🌞Ajouter les routes statiques nécessaires pour que node1.net1.tp2 et marcel.net2.tp2 puissent se ping : 

dans ma vm Node.1
```
$ sudo nano /etc/sysconfig/network-scripts/route-enp0s8

10.2.2.0/24 via 10.2.1.254 dev enp0s8
```
```
$ sudo nano /etc/sysconfig/network-scripts/route-enp0s8

10.2.1.0/24 via 10.2.2.254 dev enp0s8
```
```
$ ping 10.2.2.12
PING 10.2.2.12 (10.2.2.12) 56(84) bytes of data.
64 bytes from 10.2.2.12: icmpseq=1 ttl=63 time=2.22 ms
64 bytes from 10.2.2.12: icmpseq=2 ttl=63 time=1.43 ms
64 bytes from 10.2.2.12: icmpseq=3 ttl=63 time=1.48 ms
64 bytes from 10.2.2.12: icmpseq=4 ttl=63 time=2.15 ms
^C
--- 10.2.2.12 ping statistics ---
4 packets transmitted, 4 received, 0% packet loss, time 3008ms
rtt min/avg/max/mdev = 1.425/1.816/2.218/0.368 ms
```
```
$ ping 10.2.1.11
PING 10.2.1.11 (10.2.1.11) 56(84) bytes of data.
64 bytes from 10.2.1.11: icmpseq=1 ttl=63 time=2.60 ms
64 bytes from 10.2.1.11: icmpseq=2 ttl=63 time=1.22 ms
^C
--- 10.2.1.11 ping statistics ---
2 packets transmitted, 2 received, 0% packet loss, time 1002ms
rtt min/avg/max/mdev = 1.221/1.909/2.598/0.689 ms
```

## 2. Analyse de trames : 

### 🌞Analyse des échanges ARP : 
```
$ sudo ip -s -s neigh flush all
 
10.2.2.254 dev enp0s8 lladdr 08:00:27:c1:ad:ac used 174/170/129 probes 1 STALE
10.2.2.1 dev enp0s8 lladdr 0a:00:27:00:00:00 ref 1 used 36/0/31 probes 4 REACHABLE

 Round 1, deleting 2 entries 
 Flush is complete after 1 round 

10.2.1.11 dev enp0s9 lladdr 08:00:27:70:cb:7d used 182/177/152 probes 1 STALE
10.0.2.2 dev enp0s3 lladdr 52:54:00:12:35:02 used 477/477/451 probes 4 STALE
10.2.2.12 dev enp0s8 lladdr 08:00:27:60:55:b2 used 182/177/159 probes 1 STALE
10.2.2.1 dev enp0s8 lladdr 0a:00:27:00:00:00 ref 1 used 0/0/4 probes 4 DELAY

 Round 1, deleting 4 entries 
 Flush is complete after 1 round 

10.2.1.254 dev enp0s8 lladdr 08:00:27:e9:85:fe used 188/183/159 probes 1 STALE
10.2.1.1 dev enp0s8 lladdr 0a:00:27:00:00:02 ref 1 used 0/0/2 probes 4 DELAY

 Round 1, deleting 2 entries 
 Flush is complete after 1 round 
```
```
$ ping 10.2.2.12
PING 10.2.2.12 (10.2.2.12) 56(84) bytes of data.
64 bytes from 10.2.2.12: icmpseq=1 ttl=63 time=2.34 ms
64 bytes from 10.2.2.12: icmpseq=2 ttl=63 time=1.22 ms
64 bytes from 10.2.2.12: icmpseq=3 ttl=63 time=1.79 ms
^C
--- 10.2.2.12 ping statistics ---
3 packets transmitted, 3 received, 0% packet loss, time 2004ms
rtt min/avg/max/mdev = 1.220/1.784/2.344/0.460 ms
```
marcel : 
```
$ arp -a
? (10.2.2.1) at 0a:00:27:00:00:00 [ether] on enp0s8
gateway (10.2.2.254) at 08:00:27:c1:ad:ac [ether] on enp0s8
```

node1 : 
```
$ arp -a
? (10.2.1.1) at 0a:00:27:00:00:02 [ether] on enp0s8
? (10.2.1.254) at 08:00:27:e9:85:fe [ether] on enp0s8
```

routeur1 : 
```
$ arp -a
gateway (10.0.2.2) at 52:54:00:12:35:02 [ether] on enp0s3
? (10.2.2.12) at 08:00:27:60:55:b2 [ether] on enp0s8
? (10.2.1.11) at 08:00:27:70:cb:7d [ether] on enp0s9
? (10.2.2.1) at 0a:00:27:00:00:00 [ether] on enp0s8
```

On remarque que marcel et node ne communiquent pas directement entre elles et passent par le routeur aui renvoi les requetes vers son destinataire
```
$ sudo ip -s -s neigh flush all
```
```
$ ping 10.2.2.12
PING 10.2.2.12 (10.2.2.12) 56(84) bytes of data.
64 bytes from 10.2.2.12: icmpseq=1 ttl=63 time=1.40 ms
64 bytes from 10.2.2.12: icmpseq=2 ttl=63 time=2.01 ms
^C
--- 10.2.2.12 ping statistics ---
2 packets transmitted, 2 received, 0% packet loss, time 1003ms
rtt min/avg/max/mdev = 1.398/1.702/2.007/0.307 ms
```
```
$ sudo tcpdump -i enp0s8 -w tp2routagenode1.pcap not port 22
dropped privs to tcpdump
tcpdump: listening on enp0s8, link-type EN10MB (Ethernet), capture size 262144 bytes
^C17 packets captured
19 packets received by filter
0 packets dropped by kernel
```
```
$ sudo tcpdump -i enp0s8 -w tp2routagemarcel.pcap not port 22
dropped privs to tcpdump
tcpdump: listening on enp0s8, link-type EN10MB (Ethernet), capture size 262144 bytes
^C17 packets captured
19 packets received by filter
0 packets dropped by kernel
```
```
$ sudo tcpdump -i enp0s8 -w tp2routagerouteur.pcap not port 22
dropped privs to tcpdump
tcpdump: listening on enp0s8, link-type EN10MB (Ethernet), capture size 262144 bytes
^C17 packets captured
19 packets received by filter
0 packets dropped by kernel
```
```

$ ping 10.2.2.12
PING 10.2.2.12 (10.2.2.12) 56(84) bytes of data.
64 bytes from 10.2.2.12: icmpseq=1 ttl=63 time=1.30 ms
64 bytes from 10.2.2.12: icmpseq=2 ttl=63 time=2.02 ms
64 bytes from 10.2.2.12: icmpseq=3 ttl=63 time=1.00 ms
64 bytes from 10.2.2.12: icmpseq=4 ttl=63 time=1.12 ms
64 bytes from 10.2.2.12: icmpseq=5 ttl=63 time=4.03 ms
64 bytes from 10.2.2.12: icmpseq=6 ttl=63 time=1.45 ms
^C
--- 10.2.2.12 ping statistics ---
6 packets transmitted, 6 received, 0% packet loss, time 5008ms
rtt min/avg/max/mdev = 1.003/1.819/4.031/1.041 ms
```

je n'arrivais pas a recuperer mes fichiers car impossible de joindre le serveur avec les ip corespondante : 
```
$ python3 -m http.server 8888
Serving HTTP on 0.0.0.0 port 8888 (http://0.0.0.0:8888/) ...
```
## 2. Accès internet

### 🌞Donnez un accès internet à vos machines

Node1 : 
```
$ sudo tcpdump -i enp0s8 -w tp2arp.pcap not port 22
```
Marcel : 
```
$ sudo ip route add default via 10.2.2.254 dev enp0s8
```
```
$ ping 8.8.8.8

PING 8.8.8.8 (8.8.8.8) 56(84) bytes of data.
64 bytes from 8.8.8.8: icmpseq=1 ttl=63 time=19.6 ms
64 bytes from 8.8.8.8: icmpseq=2 ttl=63 time=18.3 ms
64 bytes from 8.8.8.8: icmpseq=3 ttl=63 time=18.3 ms
```
je n'arrive pas a utiliser dig (chargement infini lors de l'utilisation de la commande)

*
